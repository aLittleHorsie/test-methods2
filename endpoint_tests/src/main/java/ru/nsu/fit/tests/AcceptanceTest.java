package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class AcceptanceTest {
    private Browser browser = null;

    private static String CHECK_EMAIL = UUID.randomUUID() + "@login.com";
    private static String CHECK_EMAIL2 = UUID.randomUUID() + "@login.com";


    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
    }

    @Test
    @Title("Create customer")
    @Description("Create customer via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() {
//        Browser browser = BrowserService.openNewBrowser();

        // login to admin cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.getElement(By.id("email")).sendKeys("admin");
        browser.getElement(By.id("password")).sendKeys("setup");

        browser.getElement(By.id("login")).click();

        // create customer
        browser.waitForElement(By.id("add_new_customer")).click(By.id("add_new_customer"));

        browser.getElement(By.id("first_name_id")).sendKeys("John");
        browser.getElement(By.id("last_name_id")).sendKeys("Weak");
        browser.getElement(By.id("email_id")).sendKeys(CHECK_EMAIL);
        browser.getElement(By.id("password_id")).sendKeys("strongpass");

        browser.getElement(By.id("create_customer_id")).click();
    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Check login")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomer() throws Exception {
        browser.getElement(By.id("table_search")).sendKeys(CHECK_EMAIL);
        String page = browser.getWebDriver().getPageSource();
        if (!page.contains(CHECK_EMAIL)) {
            throw new Exception();
        }
    }

    @Test
    @Title("Create user")
    @Description("Create user via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("User feature")
    public void createUser() {
//        Browser browser = BrowserService.openNewBrowser();

        // login to user cp
        browser.openPage("http://localhost:8080/endpoint");
        browser.waitForElement(By.id("email"));

        browser.getElement(By.id("email")).sendKeys("admin");
        browser.getElement(By.id("password")).sendKeys("setup");

        browser.getElement(By.id("login")).click();

        // create user
        browser.waitForElement(By.id("add_new_customer")).click(By.id("add_new_customer"));

        browser.getElement(By.id("first_name_id")).sendKeys("John");
        browser.getElement(By.id("last_name_id")).sendKeys("Weak");
        browser.getElement(By.id("email_id")).sendKeys(CHECK_EMAIL2);
        browser.getElement(By.id("password_id")).sendKeys("strongpass");

        browser.getElement(By.id("create_customer_id")).click();
    }

    @Test(dependsOnMethods = "createUser")
    @Title("Check user login")
    @Description("Get user id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("User feature")
    public void checkUser() throws Exception {
        browser.getElement(By.id("table_search")).sendKeys(CHECK_EMAIL2);
        String page = browser.getWebDriver().getPageSource();
        if (!page.contains(CHECK_EMAIL2)) {
            throw new Exception();
        }
    }

    @Test
    @Title("Check Existence")
    @Description("Check existance of UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Main feauture")
    public void checkHealth() throws Exception {
//        Browser browser = BrowserService.openNewBrowser();

        // login to user cp
        browser.openPage("http://localhost:8080/endpoint/rest/health_check");
        String page = browser.getWebDriver().getPageSource();
        if (!page.contains("OK")) {
            throw new Exception();
        }
    }


    @AfterClass
    public void afterClass() {
        if (browser != null)
            browser.close();
    }
}
