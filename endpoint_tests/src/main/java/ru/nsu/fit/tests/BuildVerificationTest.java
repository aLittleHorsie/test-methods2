package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.internal.util.Base64;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Title("BuildVerificationTest")
public class BuildVerificationTest {

    private String myEmail;
    private String userEmail;
    private String userKey;

    @Test
    @Title("health check")
    @Description("Checking if site is working")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Main feature")
    public void healthCheck() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest/health_check");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

    @Test(dependsOnMethods = "healthCheck")
    @Title("Create customer")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomer() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("create_customer");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        myEmail = UUID.randomUUID() + "@login.com";
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"firstName\":\"Johnds\",\n" +
                "    \"lastName\":\"Weak\",\n" +
                "    \"login\":\"" + myEmail + "\",\n" +
                "    \"pass\":\"password123\",\n" +
                "    \"money\":\"100\"\n" +
                "}", MediaType.APPLICATION_JSON));
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

//    @Test(dependsOnMethods = "createCustomer")
//    @Title("Check login")
//    @Description("Get customer id by login")
//    @Severity(SeverityLevel.CRITICAL)
//    @Features("Customer feature")
//    public void login() throws Exception {
//        ClientConfig clientConfig = new ClientConfig();
//
//        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
//        clientConfig.register( feature) ;
//
//        clientConfig.register(JacksonFeature.class);
//
//        Client client = ClientBuilder.newClient( clientConfig );
//
//        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest");
//
//        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
//        Response response = invocationBuilder.header("Authorization", "Basic YWRtaW46c2V0dXA=").get();
//        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
//        if(response.getStatus() != 200) {
//            throw new Exception();
//        }
//    }

    @Test(dependsOnMethods = "createCustomer")
    @Title("Get role")
    @Description("Get role of current user")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void getRole() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("get_role");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

    @Test(dependsOnMethods = "getRole")
    @Title("Get customers")
    @Description("Get all customers")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void getCustomers() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("get_customers");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.header("Authorization", "Basic YWRtaW46c2V0dXA=").get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

    @Test(dependsOnMethods = "getCustomers")
    @Title("Create user")
    @Description("Create user via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void createUser() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("create_user");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        userEmail = myEmail + "a";
        userKey = Base64.encodeAsString(userEmail + ":password123");
        Response response = invocationBuilder.header("Authorization", "Basic MGFlZTA0MzctNGIwMi00NjQxLWE5MzktNThlYTg0YmFlYTM2QGxvZ2luLmNvbTpwYXNzd29yZDEyMw==").post(Entity.entity("{\n" +
                "\t\"firstName\":\"Johnds\",\n" +
                "    \"lastName\":\"Weak\",\n" +
                "    \"login\":\"" + userEmail + "\",\n" +
                "    \"pass\":\"password123\",\n" +
                "    \"money\":\"100\"\n" +
                "}", MediaType.APPLICATION_JSON));
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

    @Test(dependsOnMethods = "createUser")
    @Title("Set plan")
    @Description("Setting user money")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void customerSetUserMoney() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("update_user_money").path(userEmail).path("100000");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.header("Authorization", "Basic MGFlZTA0MzctNGIwMi00NjQxLWE5MzktNThlYTg0YmFlYTM2QGxvZ2luLmNvbTpwYXNzd29yZDEyMw==").get();
//        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        System.out.println("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

    @Test(dependsOnMethods = "customerSetUserMoney")
    @Title("Set plan")
    @Description("Setting user plan by id")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void customerSetUserPlanById() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("set_user_plan").path(userEmail).path("1").path("20");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.header("Authorization", "Basic MGFlZTA0MzctNGIwMi00NjQxLWE5MzktNThlYTg0YmFlYTM2QGxvZ2luLmNvbTpwYXNzd29yZDEyMw==").get();
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }

    @Test(dependsOnMethods = "customerSetUserPlanById")
    @Title("Get user plan")
    @Description("Getting user own plan by id")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void getUserOwnPlanById() throws Exception {
        ClientConfig clientConfig = new ClientConfig();

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8081/endpoint/rest").path("get_personal_plan").path(userEmail);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.header("Authorization", "Basic " + userKey).get();
        //AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        System.out.println("Response: " + response.readEntity(String.class));
        if(response.getStatus() != 200) {
            throw new Exception();
        }
    }
}
