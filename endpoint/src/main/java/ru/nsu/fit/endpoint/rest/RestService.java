package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import static ru.nsu.fit.endpoint.service.database.DBService.deleteUserByLogin;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;



/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("/")
public class RestService {
    @RolesAllowed(Roles.UNKNOWN)
    @GET
    @Path("/health_check")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response healthCheck() {
        return Response.ok().entity("{\"status\": \"OK\"}").build();
    }

    @RolesAllowed({Roles.UNKNOWN , Roles.CUSTOMER, Roles.ADMIN})
    @GET
    @Path("/get_role")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRole(@Context ContainerRequestContext crc) {
        return Response.ok().entity(String.format("{\"role\": \"%s\"}", crc.getProperty("ROLE"))).build();
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCustomers() {
        try {
            List<Customer.CustomerData> result = DBService.getCustomers();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            DBService.createCustomer(customerData);
            return Response.ok().entity(customerData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.ok().entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/get_customer_own_id/{customer_get_own_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerOwnId(@PathParam("customer_get_own_id") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.ok().entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/get_personal/{customer_get_personal_data}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerOwnDataById(@PathParam("customer_get_personal_data") String customerLogin) {
        try {
            List<String> res = DBService.getCustomerDataByLogin(customerLogin);
            return Response.ok().entity("{" +
                    "\"id\":\"" + res.get(0) + "\",\"firstname\":\"" + res.get(1) + "\",\"lastname\":\"" + res.get(2) +
                    "\"login\":\"" + res.get(3) + "\",\"pass\":\"" + res.get(4) + "\",\"money\":\"" + res.get(5) +
                    "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/get_personal_subscription/{customer_get_subscription}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerOwnSubscriptionById(@PathParam("customer_get_subscription") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            List<String> res = DBService.getCustomerSubscriptionByLogin(id);
            return Response.ok().entity("{" +
                    "\"id\":\"" + res.get(0) + "\",\"customerId\":\"" + res.get(1) + "\",\"servicePlanId\":\"" + res.get(2) +
                    "\"usedSeats\":\"" + res.get(3) +
                    "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/get_customer_personal_plan/{customer_get_plan}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerOwnPlanById(@PathParam("customer_get_plan") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            List<String> res = DBService.getCustomerPlanByLogin(id);
            return Response.ok().entity("{" +
                    "\"name\":\"" + res.get(3) +"\",\"details\":\"" + res.get(4) +
                    "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/buy_plan/{customer_buy_plan}/{plan_id}/{num_seats}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buyCustomerPlanById(@PathParam("customer_buy_plan") String customerLogin, @PathParam("plan_id") Integer plan_id, @PathParam("num_seats") Integer num_seats) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            DBService.customerByPlanById(id,plan_id,num_seats);
            return Response.ok().entity(plan_id.toString() + " was bought with " + num_seats.toString() + " seats").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/update_money/{customer_update_money}/{money}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCustomerMoneyByLogin(@PathParam("customer_update_money") String customerLogin, @PathParam("money") Integer money) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            DBService.customerUpdateMoney(id,money);
            return Response.ok().entity("You've added " + money.toString() + "to your account").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    //****
    //USER
    //****

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/get_users")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        try {
            List<User.UserData> result = DBService.getUsers();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @POST
    @Path("/create_user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(String userDataJson) {
        try {
            User.UserData userData = JsonMapper.fromJson(userDataJson, User.UserData.class);
            DBService.createUser(userData);
            return Response.ok().entity(userData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/get_user_id/{user_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserId(@PathParam("user_login") String userLogin) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            return Response.ok().entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.USER)
    @GET
    @Path("/get_user_own_id/{user_get_own_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUSerOwnId(@PathParam("user_get_own_id") String userLogin) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            return Response.ok().entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.USER)
    @GET
    @Path("/get_personal_own_data/{user_get_personal_data}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserOwnDataById(@PathParam("user_get_personal_data") String customerLogin) {
        try {
            List<String> res = DBService.getCustomerDataByLogin(customerLogin);
            return Response.ok().entity("{" +
                    "\"id\":\"" + res.get(0) + "\",\"firstname\":\"" + res.get(1) + "\",\"lastname\":\"" + res.get(2) +
                    "\"login\":\"" + res.get(3) + "\",\"pass\":\"" + res.get(4) + "\",\"money\":\"" + res.get(5) +
                    "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/delete_user/{user_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("user_login") String userLogin) {
        try {
            deleteUserByLogin(userLogin);
            return Response.ok().entity(userLogin +" was deleted").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/set_user_role/{user_role}/{role_name}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setUserRoleById(@PathParam("user_role") UUID id, @PathParam("role_name")User.UserRole userRole) {
        try {
            setUserRoleById(id, userRole);
            return Response.ok().entity("User's role was changed").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/update_user_money/{user_update_money}/{money}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserMoneyByLogin(@PathParam("user_update_money") String userLogin, @PathParam("money") Integer money) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            DBService.userUpdateMoney(id,money);
            return Response.ok().entity("You've added " + money.toString() + "to your account").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.CUSTOMER)
    @GET
    @Path("/set_user_plan/{customer_set_users_plan}/{plan_id}/{num_seats}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response customerSetUserPlanById(@PathParam("customer_set_users_plan") String userLogin, @PathParam("plan_id") Integer plan_id, @PathParam("num_seats") Integer num_seats) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            DBService.userByPlanById(id,plan_id,num_seats);
            return Response.ok().entity(plan_id.toString() + " was set for " + userLogin).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.USER)
    @GET
    @Path("/get_personal_plan/{user_get_plan}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserOwnPlanById(@PathParam("user_get_plan") String userLogin) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            List<String> res = DBService.getUserPlanByLogin(id);
            return Response.ok().entity("{" +
                    "\"name\":\"" + res.get(0) +"\",\"details\":\"" + res.get(1) +
                    "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}