package ru.nsu.fit.endpoint.service.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER     = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) " +
            "values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String SELECT_CUSTOMER     = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS    = "SELECT * FROM CUSTOMER";

    private static final String SELECT_CUSTOMER_DATA= "SELECT * FROM CUSTOMER WHERE login='%s'";

    private static final String SELECT_CUSTOMER_SUBSCRIPTION = "SELECT * FROM SUBSCRIPTION WHERE customerId='%s'";

    private static final String SELECT_CUSTOMER_PLAN    = "SELECT customerId, servicePlanId FROM SUBSCRIPTION " +
            "LEFT JOIN ServicePlan ON (SUBSCRIPTION.servicePlanId = ServicePlan.id) WHERE SUBSCRIPTION.customerId='%s' GROUP BY ServicePlan.id";

    private static final String SELECT_CUSTOMER_MONEY   = "SELECT money FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_PLAN_COST        = "SELECT feePerUnit FROM ServicePlan WHERE id='%s'";
    private static final String INSERT_SUBSCRIPTION     = "INSERT INTO SUBSCRIPTION(userId, servicePlanId, usedSeats) " +
            "values ('%s', '%s', '%s')";

    private static final String UPDATE_CUSTOMER         = "UPDATE CUSTOMER SET money ='%s' WHERE id='%s'";


    private static final String INSERT_USER     = "INSERT INTO USER(ID, FirstName, LastName, Login, Password, Money) " +
            "values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String SELECT_USER     = "SELECT id FROM USER WHERE login='%s'";
    private static final String SELECT_USERS    = "SELECT * FROM USER";
    private static final String DELETE_USER     = "DELETE FROM USER WHERE Login='%s' LIMIT 1";
    private static final String UPDATE_USER_ROLE= "UPDATE USER SET roleName ='%s' WHERE id='%s'";
    private static final String SELECT_USER_ROLE= "SELECT userRole FROM USER WHERE login='%s'";

    private static final String SELECT_USER_MONEY   = "SELECT money FROM USER WHERE id='%s'";

    private static final String SELECT_USER_PLAN    = "SELECT Plan.Name, Plan.Details FROM SUBSCRIPTION " +
            "LEFT JOIN ServicePlan as Plan ON (SUBSCRIPTION.servicePlanId = Plan.ID) WHERE SUBSCRIPTION.userId='%s' GROUP BY Plan.ID";

    private static final String UPDATE_USER         = "UPDATE USER SET money ='%s' WHERE id='%s'";

    /*Auth*/
    private static final String AUTH_USER     = "SELECT * FROM User WHERE login='%s' and password='%s'";
    private static final String AUTH_CUSTOMER = "SELECT * FROM Customer WHERE login='%s' and pass='%s'";

    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");
    private static final Object generalMutex = new Object();
    private static Connection connection;

    static {
        init();
    }






    public static boolean authUser(String username, String password)
    {
        synchronized (generalMutex) {
            logger.info("Try to see if it is user");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                AUTH_USER,
                                username,password));
                if (rs.next()) {
                    return true;
                } else {
                    throw new IllegalArgumentException("User with login '" + username + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static boolean authCustomer(String username, String password)
    {
        synchronized (generalMutex) {
            logger.info("Try to see if it is customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                AUTH_CUSTOMER,
                                username,password));
                if (rs.next()) {
                    return true;
                } else {
                    //throw new IllegalArgumentException("Customer with login '" + username + " was not found");
                    return false;
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPass(),
                                customer.getData().getMoney()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<Customer.CustomerData> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer.CustomerData> result = Lists.newArrayList();
                while (rs.next()) {
                    result.add(new Customer.CustomerData(
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getInt(6)));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<String> getCustomerDataByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get customer personal data");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_DATA,
                                customerLogin));
                List<String> result = new ArrayList<>();
                Integer k = 0;
                while (rs.next()) {
                    k++;
                    result.add(rs.getString(k));
                }
                return result;

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<String> getCustomerSubscriptionByLogin(UUID id) {
        synchronized (generalMutex) {
            logger.info("Try to get customer personal subscriptions");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_SUBSCRIPTION,
                                id));
                List<String> result = new ArrayList<>();
                Integer k = 0;
                while (rs.next()) {
                    k++;
                    result.add(rs.getString(k));
                }
                return result;

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<String> getCustomerPlanByLogin(UUID id) {
        synchronized (generalMutex) {
            logger.info("Try to by plan");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_PLAN,
                                id));
                List<String> result = new ArrayList<>();
                Integer k = 0;
                while (rs.next()) {
                    k++;
                    result.add(rs.getString(k));
                }
                return result;

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void customerByPlanById(UUID id, Integer id_cost, Integer num_seats) {
        synchronized (generalMutex) {
            logger.info("Try to get customer personal plans");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_MONEY,
                                id));
                Integer money = 0;
                while (rs.next()) {
                    money = new Integer(rs.getString(1));
                }

                ResultSet rs2 = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_COST,
                                id_cost));

                Integer cost = 0;
                while (rs.next()) {
                    cost = new Integer(rs.getString(1));
                }

                if (num_seats * cost <= money){
                    statement.executeUpdate(
                            String.format(
                                    INSERT_SUBSCRIPTION,
                                    id,
                                    id_cost,
                                    num_seats));
                } else {
                    throw new IllegalArgumentException("Not enough money");
                }

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void customerUpdateMoney(UUID id, Integer money) {
        synchronized (generalMutex) {
            logger.info("Try to add money");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER,
                                id,money));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


// USER
    public static void createUser(User.UserData userData) {
        synchronized (generalMutex) {
            logger.info("Try to create user");

            User user = new User(userData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_USER,
                                user.getId(),
                                user.getData().getFirstName(),
                                user.getData().getLastName(),
                                user.getData().getLogin(),
                                user.getData().getPass(),
                                user.getData().getMoney()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<User.UserData> getUsers() {
        synchronized (generalMutex) {
            logger.info("Get users");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_USERS);
                List<User.UserData> result = Lists.newArrayList();
                while (rs.next()) {
                    result.add(new User.UserData(
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getInt(6)));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getUserIdByLogin(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to create user");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER,
                                userLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("User with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void deleteUserByLogin(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to delete user");

            try {
                Statement statement = connection.createStatement();
                statement.executeQuery(
                        String.format(
                                DELETE_USER,
                                userLogin));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void setUserRoleById(UUID id, User.UserRole userRole) {
        synchronized (generalMutex) {
            logger.info("Try to set user role");

            try {
                Statement statement = connection.createStatement();
                statement.executeQuery(
                        String.format(
                                UPDATE_USER_ROLE,
                                userRole, id));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static String getUserRoleByLogin(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get users role");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_ROLE,
                                userLogin));
                if (rs.next()) {
                    return rs.getString(1);
                } else {
                    throw new IllegalArgumentException("User with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    public static void userByPlanById(UUID id, Integer id_cost, Integer num_seats) {
        synchronized (generalMutex) {
            logger.info("Try to get user personal plans");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_MONEY,
                                id));
                Integer money = 0;
                while (rs.next()) {
                    money = new Integer(rs.getString(1));
                }

                ResultSet rs2 = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_COST,
                                id_cost));

                Integer cost = 0;
                while (rs2.next()) {
                    cost = new Integer(rs2.getString(1));
                }

                if (num_seats * cost <= money){
                    statement.executeUpdate(
                            String.format(
                                    INSERT_SUBSCRIPTION,
                                    id,
                                    id_cost,
                                    num_seats));
                } else {
                    throw new IllegalArgumentException("Not enough money");
                }

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<String> getUserPlanByLogin(UUID id) {
        synchronized (generalMutex) {
            logger.info("Try to get plan");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_PLAN,
                                id));
                List<String> result = new ArrayList<>();
                while (rs.next()) {
                    result.add(rs.getString(1));
                    result.add(rs.getString(2));
                }
                return result;

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    public static void userUpdateMoney(UUID id, Integer money) {
        synchronized (generalMutex) {
            logger.info("Try to add money");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_USER,
                                money, id));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/JavaTest?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "root",
                            "user");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }


}
